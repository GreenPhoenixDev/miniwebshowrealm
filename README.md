# Mini Show Realm

At first this was supposed to be a Show Case application, to showcase objects inside a unity scene through a web player. 
Though Unity does not WebBuild Support for the HDRP  so I repurposed it for showcsing the particle effects i made for my FinalExamProject.

So here is a little video of the particle effects I made.

![ParticleEffects Video](https://imgur.com/0nOrxgM.mp4)